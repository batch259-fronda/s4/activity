<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S04 Activity</title>
	</head>
	<body>
		<?php session_start(); ?>
		<?php if(isset($_SESSION['username'])): ?>
			<p>Hello, <?php echo $_SESSION['username']; ?></p>
			<form method="POST" action="./server.php">
				<input type="hidden" name="action" value="logout">
				<button type="submit">Logout</button>
			</form>

		<?php else: ?>
			<form method="POST" action="./server.php">
				<input type="hidden" name="action" value="login">
				Username: <input type="text" name="username" required>
				Password: <input type="password" name="password" required>
				<button type="submit">Login</button>
			</form>
		<?php endif; ?>
	</body>
</html>
<?php

session_start();

if(isset($_POST['action'])) {
	if($_POST['action'] === 'login') {
		$username = $_POST['username'];
		$password = $_POST['password'];
		$_SESSION['username'] = $username;
	}

	else if($_POST['action'] === 'logout') {
		session_destroy();
	}
}

header('Location: index.php');